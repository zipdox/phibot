# PhiBot
PhiBot is a Minecraft bot I wrote in NodeJS to run on anarchy servers. It's primarity meant for constantiam.net but the server it runs on doesn't matter.

# License
PhiBot is licensed under the GNU AGPL version 3. As such, if you run PhiBot, or part of it, on a server, you are required to distribute your source code.
