var say;
function git(){
    say("PhiBot's Git repo: https://gitlab.com/zipdox/phibot");
}

module.exports = function(sayFunc){
    say = sayFunc;
    return git;
}
